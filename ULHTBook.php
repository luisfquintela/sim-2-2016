<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistema de Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    
    <?php

      session_start();

      //Se não existir login
      if(empty($_SESSION['idUtilizador']))
            header("Location:index.php");

      require("nav.php");


      //Se for para inserir publicação
      if (isset($_GET['action']) && $_GET['action'] == 'inserePublicacao') {
          $pdo = ligacaoBD();
          publicarFeed($pdo,$_SESSION['idUtilizador'],$_POST['descricao']);
          terminaLigacaoBD($pdo);
          echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Publicação inserida com sucesso.</div></div></div>";
        }


    ?>

    <div class="container">
      <div class="col-md-12">
        <div class="row">

        <div class="panel panel-primary">
          <div class="panel-heading">Publicação</div>
            <div class="panel-body">
              <form action="?action=inserePublicacao" method="post">
                <div class="form-group">
                  <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Mensagem">
                </div>
                <button type="submit" class="btn btn-primary  pull-right">Publicar</button>
            </form>
          </div>
        </div>

        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
