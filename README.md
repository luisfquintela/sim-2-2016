## GRUPO ##
 - 20064896 - Carlos Cunha
 - 21604345 - Luis Quintela
 - 21403301 - Rui Correia

##Visual##
Foi utilizado a framework bootstrap para todo o design gráfico da aplicação, visto já ter grande parte do trabalho realizado.

##Base de Dados##
Foi utilizado o mysql como sistema de gestão de base de dados.
A nossa base de dados está simples, visto não ser uma aplicação real com questões de segurança etc.
Temos as seguintes tabelas:
 - utilizador (onde estão os dados de cada utilizador)
 - pedidoamizade (onde ficam os pedidos de amizade pendentes de aceitação por outro utilizador)
 - amizade (onde fica registado todos os amigos)
 - feed (feed de noticias)

##Inicio de Sessão##
Estamos a utilizar os conceitos de sessão do PHP ($_SESSION) para guardar o id do utilizador e guardar as suas permissões. Estes campos são fundamentais para o restante código executar correctamente.

##Ligação Base de Dados##
Toda a ligação á base de dados tem um ficheiro (baseDados.php) com todas as funções necessárias.

##Util##
Todos as funções que se repetem ao logo do código estão num ficheiro (útil.php)

##Submits##
Para não fazermos uma página para cada submit visto que fica muito confuso quando fazemos algum submit ele coloca um GET no URL (action=accção) e quando recarrega a página verifica a acção que tem de executar.

##Perfil##
A página perfil.php permite alterar todos os campos se for administrador ou se for o proprio utilizador. se não for administrador apenas permite alterar os dados do utilizador com o login. se for Guest ou um utilizador regular sem amizades apenas consegue ver a fotografia e o nome sem conseguir submeter nada.
A página permite ainda inserir um novo utilizador, alterando alguns campos e adicionando outro.