<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Amigos</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    
      <?php

      session_start();

      if(empty($_SESSION['idUtilizador']))
            header("Location:index.php");


      require("nav.php");     

      // Verifica se é para eliminar amizade
      if (isset($_GET['action']) && $_GET['action'] == 'eliminaAmizade') {
          $pdo = ligacaoBD();
          eliminaAmizade($pdo,$_SESSION['idUtilizador'],$_GET["id"]);
          terminaLigacaoBD($pdo);
          echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Amizade eliminada com sucesso</div></div></div>";
      } 

      $pdo = ligacaoBD();
      $amigos = daListaAmigos($pdo,$_SESSION["idUtilizador"]);
      terminaLigacaoBD($pdo);

      
  array_values($amigos);

      if(!empty($amigos)):
        ?>

   <h2 align="center">Os teus amigos</h2>
  <div class="container">
  <div class="row">
    <div class="col-md-12">      
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <th>Imagem</th>
              <th>Nome</th>  
              <th>Eliminar Amizade</th>       
            </thead>
            <tbody>
              <?php
              

              foreach($amigos as $listaAmigos)
              {              
                
                
                echo "<td><img src=".$listaAmigos["Imagem"]." height='80' width='80'border='1'></td>";
                echo "<br>";
                echo "<td>".$listaAmigos["Nome"]."</td>";
                echo "<td><a  href='?id=".$listaAmigos["IDUtilizador"]."&action=eliminaAmizade'><button class='btn btn-danger'>Eliminar Amizade</button></a></td>";	               
                echo "<tr>";    
              }                  
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php
  else:
    {    
      echo "<div class='container'><div class='col-md-12'><div class='alert alert-warning' role='alert'>Infelizmente não tens amigos, começa já a adicionar <a href='pedidosAmizade.php'>aqui</a></div></div></div>";
    }
  endif
  ?>
  

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>