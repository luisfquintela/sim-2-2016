<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Amizade</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    
      <?php

      session_start();

      //Se não existir login
      if(empty($_SESSION['idUtilizador']))
            header("Location:index.php");


      require("nav.php");


      if (isset($_GET['action']) && $_GET['action'] == 'addAmigo') {
            $pdo = ligacaoBD();
            adicionaPedidoAmizade($pdo,$_GET["idAmigo"],$_SESSION["idUtilizador"]);
            terminaLigacaoBD($pdo);
            echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Pedido de amizade efectuado</div></div></div>";
		}

    if (isset($_GET['action']) && $_GET['action'] == 'acceptAmigo') {
            $pdo = ligacaoBD();
            aceitaPedidoAmizade($pdo,$_GET["idAmigo"],$_SESSION["idUtilizador"]);            
            terminaLigacaoBD($pdo);
            echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Parabéns! São agora amigos.</div></div></div>";
    }



      $pdo = ligacaoBD();
      $perfil = daListaUtilizadores($pdo,$_SESSION["idUtilizador"]);
      $pedidos = daListaPedidos($pdo,$_SESSION["idUtilizador"]);
      terminaLigacaoBD($pdo);

      
  

      if(!empty($pedidos)):
        ?>

   <h1 align="center">Pedidos Pendentes:</h1>
  <div class="container">
  <div class="row">
    <div class="col-md-12">      
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <th>Imagem</th>
              <th>Nome</th>             
            </thead>
            <tbody>
              <?php

              foreach($pedidos as $pedidosAmizade)
              {              
                echo "<td><img src=".$pedidosAmizade["Imagem"]." height='80' width='80'border='1'></td>";
                echo "<br>";
                echo "<td>".$pedidosAmizade["Nome"]."</td>";
                echo "<td>" . " <a href='?action=acceptAmigo&idAmigo=". $pedidosAmizade["idutilizador"] ."' class='btn btn-primary pull-right'>Aceitar</a>" . "</td>";
                echo "<tr>";    
              }                  
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php
  endif;
  
  if(!empty($perfil)):
  ?>
    
  <h1 align="Center">Adicionar Amizade:</h1>
  <div class="container">
    <div class="row">
      <div class="col-md-12">        
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <th>Imagem</th>
                <th>Nome</th>
                <th>Adicionar</th>               
              </thead>
              <tbody>
                <?php
                  foreach($perfil as $utilizador)
                  {              
                    echo "<td><img src=".$utilizador["Imagem"]." height='80' width='80'border='1'></td>";
                    echo "<br>";
                    echo "<td>".$utilizador["Nome"]."</td>";
                    echo "<td>" . " <a href='?action=addAmigo&idAmigo=" . $utilizador["IDUtilizador"] . "' class='btn btn-primary'>Adicionar</a>" . "</td>";
                    echo "<tr>";    
                  }                  
                ?>
              </tbody>
            </div>
      </div>
    </div>
  </div>

  <?php
  endif;  
  ?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>