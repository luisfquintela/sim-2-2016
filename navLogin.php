<nav class="navbar navbar-dark bg-primary navbar-fixed-top">
			<div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">ULHTBook</a>
		    </div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">	
						<form class="navbar-form navbar-right" action="login.php" method="POST">
							<div class="form-group">
								<input name="utilizador" type="text" class="form-control" placeholder="Utilizador">
								<input name="password" type="password" class="form-control" placeholder="Password">
							</div>
							<button type="submit" class="btn btn-default">Entrar</button>
							<?php 
								if(!empty($_SESSION['loginError']))
									if($_SESSION['loginError']>0)
										echo ' <span class="label label-danger">Utilizador ou password incorrectos</span>' ;		
							?> 
						</form>
						<ul class="nav navbar-nav navbar-right">		
					 		<li><a href="listaUtilizadores.php">Lista utilizadores</a></li>	
					 </ul>
				</div>
	  	</div>
		</nav>
