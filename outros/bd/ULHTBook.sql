-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Dez-2016 às 21:40
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulhtbook`
--
CREATE DATABASE IF NOT EXISTS `ulhtbook` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ulhtbook`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `amizade`
--

DROP TABLE IF EXISTS `amizade`;
CREATE TABLE `amizade` (
  `IDUtilizador_A` int(11) NOT NULL,
  `IDUtilizador_B` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `amizade`
--

INSERT INTO `amizade` (`IDUtilizador_A`, `IDUtilizador_B`) VALUES
(6, 1),
(1, 2),
(10, 3),
(9, 5),
(4, 6),
(2, 8),
(1, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `feed`
--

DROP TABLE IF EXISTS `feed`;
CREATE TABLE `feed` (
  `ID` int(11) NOT NULL,
  `IDUtilizador` int(11) NOT NULL,
  `Data` datetime NOT NULL DEFAULT '2016-12-14 00:00:00',
  `Descricao` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidoamizade`
--

DROP TABLE IF EXISTS `pedidoamizade`;
CREATE TABLE `pedidoamizade` (
  `IDUtilizador_Pede` int(11) NOT NULL,
  `IDUtilizador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pedidoamizade`
--

INSERT INTO `pedidoamizade` (`IDUtilizador_Pede`, `IDUtilizador`) VALUES
(1, 7),
(1, 11),
(2, 4),
(2, 9),
(2, 10),
(3, 6),
(3, 7),
(3, 9),
(4, 1),
(4, 7),
(4, 10),
(5, 1),
(5, 2),
(5, 6),
(5, 7),
(5, 10),
(6, 2),
(6, 7),
(6, 8),
(6, 9),
(8, 1),
(8, 4),
(9, 7),
(9, 8),
(10, 6),
(10, 7),
(11, 4),
(11, 6),
(11, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `utilizador`
--

DROP TABLE IF EXISTS `utilizador`;
CREATE TABLE `utilizador` (
  `IDUtilizador` int(11) NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Nome` varchar(70) NOT NULL,
  `Telefone` int(20) NOT NULL,
  `DataNascimento` date NOT NULL,
  `Sexo` char(1) NOT NULL,
  `Descricao` text,
  `Morada` varchar(60) NOT NULL,
  `Imagem` text NOT NULL,
  `Administrador` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `utilizador`
--

INSERT INTO `utilizador` (`IDUtilizador`, `Email`, `Password`, `Nome`, `Telefone`, `DataNascimento`, `Sexo`, `Descricao`, `Morada`, `Imagem`, `Administrador`) VALUES
(1, 'luis.quintela@basic.pt', '1234', 'Luis Maria dos Santos Quintela', 923456789, '2016-10-17', 'H', 'Admin user - Geek Professional', 'Rua dos Geeks NÂº 1', 'http://az616578.vo.msecnd.net/files/2015/12/05/635848891636330946-886371098_70522612-650d-414d-80dd-736e7ac1132f.jpg', 1),
(2, 'jorge100@gmail.com', '1234', 'Carlos Jorge Nunes Filipe dos Santos Pedro', 923456789, '1976-06-11', 'H', '46 anos em cada perna.', 'Rua do Brasil NÂº49', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/111863/profile/profile-512_6.jpg', 1),
(3, 'rui.pedro.correia@celfocus.com', '1234', 'Rui Maria GuinÃ© de Fonseca', 912198747, '1987-10-17', 'H', 'Admin user - Novabase', 'Rua da casa amarela NÂº36', 'https://www.photoshopgurus.com/forum/attachments/photoshop-contests/2970d1302075464t-avatar-wars-crazy_face_glasses_2-jpg', 0),
(4, 'pedro@gmail.com', '1234', 'Pedro', 918652398, '1965-12-07', 'H', 'Tester', 'Pedro Patricio Rodrigues Pereira da Cunha', 'https://cdn0.iconfinder.com/data/icons/iconshock_guys/512/andrew.png', 0),
(5, 'susana@gmail.com', '1234', 'Susana Maria Sofia de Jesus', 916678963, '1988-12-07', 'M', 'Desempregada', 'Rua da Republica de Angola NÂº69', 'https://1.bp.blogspot.com/-Yqtn9LddHjM/V3KixI4oT5I/AAAAAAAAAo8/Wbcei-C9A2M2CZ2BqDFSJ8wmBQQMae5SgCKgB/s1600/803711349_41842.jpg', 0),
(6, 'ana@gmail.com', '1234', 'Ana Janise Pedro', 928653269, '1975-12-07', 'M', 'SecretÃ¡ria  Novabase', 'Rua do moinho NÂº45', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png', 0),
(7, 'maria@gmail.com', '1234', 'Maria Albertina dos Santos ConceiÃ§Ã£o Figueiredo Rodrigues', 215869365, '2016-12-07', 'M', 'Blogger', 'Rua dos girassÃ³is', 'https://pickaface.net/gallery/avatar/madridlover53614c24455e8.png', 0),
(8, 'luisa@gmail.com', '1234', 'Luisa Virgem dos Santos', 912147147, '2001-10-07', 'M', 'Comercial', 'Rua da Cepa Torta NÂº11', 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT7V3vJwu08CLBIXGyxPkphMIdLxgwwltf0CsIRB-LDW6JlsMjCXw', 0),
(9, 'celia@gmail.com', '1234', 'Celia Dos Santos Careca', 912198321, '1985-11-10', 'M', 'Freelancer', 'UrbanizaÃ§Ã£o Nova Bairro J zona P', 'http://images.teamsugar.com/files/upl1/2/20652/21_2008/avatar-xlarge-15111.jpg', 0),
(10, 'paula@gmail.com', '1234', 'Paula ConceiÃ§Ã£o Rodrigues Pereira', 931195421, '1923-01-10', 'M', 'RelaÃ§Ãµes PÃºblicas', 'Rua Batinotone NÂº 35', 'http://www.pd4pic.com/images/girl-woman-female-avatar-design.png', 0),
(11, 'paulo.faria@hp.com', '1234', 'Paulo dos Santos Faria', 911125365, '1985-10-22', 'H', 'Freelancer Vodafone', 'Quinta da Fonte NÂº 7', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRmje_TGwq1edGRZ2jdWQchMST_4qeLYu9XCyj7TrQImZN-dsiJ-g', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amizade`
--
ALTER TABLE `amizade`
  ADD PRIMARY KEY (`IDUtilizador_B`);

--
-- Indexes for table `feed`
--
ALTER TABLE `feed`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `IDUtilizador` (`IDUtilizador`);

--
-- Indexes for table `pedidoamizade`
--
ALTER TABLE `pedidoamizade`
  ADD PRIMARY KEY (`IDUtilizador_Pede`,`IDUtilizador`);

--
-- Indexes for table `utilizador`
--
ALTER TABLE `utilizador`
  ADD PRIMARY KEY (`IDUtilizador`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feed`
--
ALTER TABLE `feed`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `utilizador`
--
ALTER TABLE `utilizador`
  MODIFY `IDUtilizador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
