<?php


function ligacaoBD(){
    try {
        $pdo = new PDO('mysql:host=localhost;dbname=ULHTBook','root','');
        return $pdo;
    } catch(PDOException $exception) {
        print $exception->getMessage();
        return false;
    }
}

function terminaLigacaoBD($pdo){
    $pdo = null;
}

function login($pdo, $utilizador, $password){
    $stmt = $pdo->prepare("SELECT IDUtilizador, Administrador FROM Utilizador WHERE Email=? and Password=?");
    $stmt->bindParam(1, $utilizador);
    $stmt->bindParam(2, $password);
    
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    
    foreach($result as $resultado){
        return array($resultado["IDUtilizador"], $resultado["Administrador"]);
    }
    
    return 0;
}

function daNome($pdo, $idUtilizador){
    $stmt = $pdo->prepare("SELECT Nome FROM Utilizador WHERE IDUtilizador=?");
    $stmt->bindParam(1, $idUtilizador);
    
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    
    foreach($result as $resultado){
        return $resultado["Nome"];
    }
    
    return 0;
}

function daInfoPerfil($pdo, $idUtilizador) {
    $stmt = $pdo->prepare("SELECT Nome, Morada, Telefone, DataNascimento, Sexo, Descricao, Email, Password, Imagem FROM Utilizador WHERE IDUtilizador=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->execute();
    $result = $stmt->fetchAll();
    
    foreach($result as $resultado){
        return array($resultado["Nome"], $resultado["Morada"], $resultado["Telefone"], $resultado["DataNascimento"], $resultado["Sexo"], $resultado["Descricao"], $resultado["Email"], $resultado["Password"],$resultado["Imagem"]);
    }
}

function daTodosUtilizadores($pdo){
    $statement = $pdo->prepare("SELECT Nome, Telefone, DataNascimento, Sexo, Descricao, Morada, Email, Imagem, IDUtilizador  FROM utilizador");
    $statement->execute();
    $todosUtilizadores = $statement->fetchAll();
    return $todosUtilizadores;
    
}

function daListaPedidos($pdo, $idUtilizador)
{
    $stmt = $pdo->prepare("SELECT Utilizador.idutilizador, utilizador.Imagem, utilizador.Nome FROM pedidoamizade INNER JOIN utilizador
    on pedidoamizade.IDUtilizador_Pede = utilizador.IDUtilizador WHERE pedidoamizade.IDUtilizador = ?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->execute();
    $resultPedidos = $stmt->fetchAll();
    return $resultPedidos;
    
}

function daListaAmigos($pdo, $idUtilizador)
{
    $stmt = $pdo->prepare("SELECT distinct(IDUtilizador), Nome, Imagem FROM Utilizador WHERE idUtilizador IN (select IDUtilizador_A from Amizade where IDUtilizador_B = ?) OR IDUtilizador IN (select IDUtilizador_B from Amizade where IDUtilizador_A = ?)");
    
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizador);
    
    
    $stmt->execute();
    $result = $stmt->fetchAll();
    return $result;
    
}

function daListaUtilizadores($pdo, $idUtilizador)
{
    $stmt = $pdo->prepare("SELECT IDUtilizador, Nome, Imagem FROM Utilizador
    WHERE Utilizador.IDUtilizador!=? AND
    (SELECT COUNT(*) FROM Amizade WHERE Amizade.IDUtilizador_A=? AND Amizade.IDUtilizador_B=Utilizador.IDUtilizador) = 0 AND
    (SELECT COUNT(*) FROM Amizade WHERE Amizade.IDUtilizador_A=Utilizador.IDUtilizador AND Amizade.IDUtilizador_B=?) = 0 AND
    (SELECT COUNT(*) FROM PedidoAmizade WHERE PedidoAmizade.IDUtilizador=Utilizador.IDUtilizador AND PedidoAmizade.IDUtilizador_Pede=?) = 0 AND
    (SELECT COUNT(*) FROM PedidoAmizade WHERE PedidoAmizade.IDUtilizador=? AND PedidoAmizade.IDUtilizador_Pede=Utilizador.IDUtilizador) = 0 ");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizador);
    $stmt->bindParam(3, $idUtilizador);
    $stmt->bindParam(4, $idUtilizador);
    $stmt->bindParam(5, $idUtilizador);
    $stmt->execute();
    $result = $stmt->fetchAll();
    return $result;
    
}

function adicionaPedidoAmizade($pdo, $idAmigo, $idUtilizador)
{
    $stmt = $pdo->prepare("INSERT INTO PedidoAmizade VALUES(?, ?) ");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idAmigo);
    $stmt->execute();
}

function aceitaPedidoAmizade($pdo, $idAmigo, $idUtilizador)
{
    $stmt = $pdo->prepare("INSERT INTO Amizade VALUES(?, ?) ");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idAmigo);
    $stmt->execute();
    removePedidoAmizadeAceite($pdo, $idAmigo, $idUtilizador);
}

function removePedidoAmizadeAceite($pdo, $idAmigo, $idUtilizador)
{
    $stmt = $pdo->prepare("DELETE FROM PedidoAmizade WHERE IDUtilizador =? AND IDUtilizador_Pede=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idAmigo);
    $stmt->execute();
    
}



function alteraInicioSessao($pdo, $idUtilizador, $user, $pass){
    $stmt = $pdo->prepare("UPDATE Utilizador SET Email=?, Password=? WHERE IDUtilizador=?");
    $stmt->bindParam(1, $user);
    $stmt->bindParam(2, $pass);
    $stmt->bindParam(3, $idUtilizador);
    $stmt->execute();
}

function alteraDadosPessoais($pdo, $idUtilizador, $nome, $morada, $telefone, $dataNascimento, $sexo, $descricao, $imagem){
    $stmt = $pdo->prepare("UPDATE Utilizador SET Nome=?, Morada=?, Telefone=?, DataNascimento=?, Sexo=?, Descricao=?, Imagem=? WHERE IDUtilizador=?");
    $stmt->bindParam(1, $nome);
    $stmt->bindParam(2, $morada);
    $stmt->bindParam(3, $telefone);
    $stmt->bindParam(4, $dataNascimento);
    $stmt->bindParam(5, $sexo);
    $stmt->bindParam(6, $descricao);
    $stmt->bindParam(7, $imagem);
    $stmt->bindParam(8, $idUtilizador);
    $stmt->execute();
}


function verificaAmizade($pdo, $idUtilizador, $idUtilizadorAmigo){
    $stmt = $pdo->prepare("SELECT COUNT(*) AS Amizade FROM Amizade WHERE (IDUtilizador_A=? and IDUtilizador_B=?) OR (IDUtilizador_A=? and IDUtilizador_B=?)");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizadorAmigo);
    $stmt->bindParam(3, $idUtilizadorAmigo);
    $stmt->bindParam(4, $idUtilizador);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    
    foreach($result as $resultado){
        return $resultado["Amizade"];
    }
    
    return 0;
}


function eliminaUtilizador($pdo, $idUtilizador){
    
    $stmt = $pdo->prepare("DELETE FROM Amizade WHERE IDUtilizador_A=? OR IDUtilizador_B=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizador);
    $stmt->execute();
    
    $stmt = $pdo->prepare("DELETE FROM PedidoAmizade WHERE IDUtilizador=? OR IDUtilizador_pedr=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizador);
    $stmt->execute();
    
    $stmt = $pdo->prepare("DELETE FROM Utilizador WHERE IDUtilizador=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->execute();
}

function criaNovoUtilizador($pdo, $email, $Password,$nome,$telefone,$dataNascimento,$sexo,$descricao,$Morada,$imagem,$Administrador) {
    $stmt = $pdo->prepare("INSERT INTO utilizador (Email, Password, Nome, Telefone, DataNascimento, Sexo, Descricao, Morada, Imagem, Administrador) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
    $stmt->bindParam(1, $email);
    $stmt->bindParam(2, $Password);
    $stmt->bindParam(3, $nome);
    $stmt->bindParam(4, $telefone);
    $stmt->bindParam(5, $dataNascimento);
    $stmt->bindParam(6, $sexo);
    $stmt->bindParam(7, $descricao);
    $stmt->bindParam(8, $Morada);
    $stmt->bindParam(9, $imagem);
    $stmt->bindParam(10, $Administrador);
    $stmt->execute();
}


function eliminaAmizade($pdo, $idUtilizador, $idUtilizadorAmigo)
{
    $stmt = $pdo->prepare("DELETE FROM Amizade WHERE IDUtilizador_A =? AND IDUtilizador_B=?");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $idUtilizadorAmigo);
    $stmt->execute();
}

function publicarFeed($pdo, $idUtilizador, $descricao)
{
    $stmt = $pdo->prepare("INSERT INTO feed (IDUtilizador,Data,Descricao) VALUES(?,now(),?)");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->bindParam(2, $descricao);
    $stmt->execute();
}

function carregaFeed($pdo, $idUtilizado)
{
    $stmt = $pdo->prepare("SELECT ");
    $stmt->bindParam(1, $idUtilizador);
    $stmt->execute();
}


?>