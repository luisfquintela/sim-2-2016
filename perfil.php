<!DOCTYPE html>
<html lang="pt">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Sistema de Login</title>

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

  <script>
    function actualizaImagem() {
      var img = document.getElementById('img');
      img.src = document.getElementById("urlFoto").value;

      img.onerror = function() {
        this.src = 'img/errorImage.png';
      };
    }
  </script>

</head>

<body>

  <?php

session_start();

//Se não existir login
        if(empty($_SESSION['idUtilizador'])){
          require("navLogin.php");
          require("baseDados.php");
          echo "<br /><br /><br /><br />";
        } else {
          require("nav.php");
        }

// Verifica se é para criar um novo utilizador
$novoUtilizador=0;
if (isset($_GET['action']) && $_GET['action'] == 'novoUtilizador') {
        $novoUtilizador=1;
        for ($i = 0; $i <= 8; $i++)
				  $perfil[$i]="";
	}

//Grava novo utilizador
  if (isset($_GET['action']) && $_GET['action'] == 'gravarNovoUtilizador') {
			$novoUtilizador=1;
			$pdo = ligacaoBD();

      $admin = 0;
      if (isset($_POST['admin']) && $_POST['admin'] == '1')
        $admin=1;

      criaNovoUtilizador($pdo,$_POST["utilizador"],$_POST["password"],$_POST["nome"],$_POST["telefone"],$_POST["dataNascimento"],$_POST["sexo"],$_POST["descricao"],$_POST["morada"],$_POST["urlFoto"],$admin);
      terminaLigacaoBD($pdo);
      echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Criado perfil com sucesso.</div></div></div>";
			exit;
		}



// Verifica se é para alterar os dados de inicio de sessão
if (isset($_GET['action']) && $_GET['action'] == 'alteraInicioSessao' && isset($_POST['utilizador']) && isset($_POST['password'])) {
    $pdo = ligacaoBD();
    alteraInicioSessao($pdo,$_GET["id"],$_POST["utilizador"],$_POST["password"]);
    terminaLigacaoBD($pdo);
    echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Dados de inicio de sessão actualizados com sucesso.</div></div></div>";
}


// Verifica se é para alterar os dados pessoais
if (isset($_GET['action']) && $_GET['action'] == 'alteraDadosPessoais' && isset($_POST["nome"]) && isset($_POST["morada"]) && isset($_POST["telefone"]) && isset($_POST["dataNascimento"]) && isset($_POST["sexo"]) && isset($_POST["urlFoto"])) {
    $pdo = ligacaoBD();
    alteraDadosPessoais($pdo,$_GET["id"],$_POST["nome"],$_POST["morada"],$_POST["telefone"],$_POST["dataNascimento"],$_POST["sexo"],$_POST["descricao"],$_POST["urlFoto"]);
    terminaLigacaoBD($pdo);
    echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Dados pessoais actualizados com sucesso.</div></div></div>";
}

// Verifica se é para eliminar o utilizador
if (isset($_GET['action']) && $_GET['action'] == 'eliminaUtilizador') {
    $pdo = ligacaoBD();
    eliminaUtilizador($pdo,$_GET["id"]);
    terminaLigacaoBD($pdo);
    echo "<div class='container'><div class='col-md-12'><div class='alert alert-success' role='alert'>Utilizador eliminado com sucesso.</div></div></div>";

    if($_GET["id"] == $_SESSION["idUtilizador"])
      terminarSessao();

    exit;
} 

// Carrega a informação do utilizador
if($novoUtilizador==0){
			$pdo = ligacaoBD();
			$perfil = daInfoPerfil($pdo,$_GET["id"]);
			terminaLigacaoBD($pdo);

    //Verifica se tem acesso a todos os dados por ser amigo
    $pdo = ligacaoBD();
    $amizade = verificaAmizade($pdo,$_SESSION['idUtilizador'],$_GET["id"]);
    terminaLigacaoBD($pdo);

    if($_SESSION['admin']==0 && $amizade==0 && $_GET["id"] != $_SESSION['idUtilizador'])
      echo "<div class='container'><div class='col-md-12'><div class='alert alert-warning' role='alert'>Informação limitada por falta de privilégios</div></div></div>";
		}

?>


    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <?php 
            if($novoUtilizador==1)
					    echo '<form action="?action=gravarNovoUtilizador" method="POST">';

            if($_SESSION["admin"] == 1 || $_GET['id']==$_SESSION["idUtilizador"]): 
            ?>
            <div class="panel panel-primary">
              <div class="panel-heading">Inicio de Sessão</div>
              <div class="panel-body">
                <?php if($novoUtilizador==0): ?>
								  <form action="<?php echo '?id=' . $_GET['id'] . '&action=alteraInicioSessao'?>" method="POST">
							  <?php endif; ?>
                  <div class="form-group">
                    <label for="utilizador">Utilizador</label>
                    <input type="text" class="form-control" id="utilizador" name="utilizador" placeholder="Utilizador" value="<?php echo $perfil[6] ?>" required="true">
                  </div>

                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $perfil[7] ?>" required="true">
                  </div>

                   <?php
                      if($novoUtilizador==0){
                        echo '<button type="submit" class="btn btn-primary pull-right">Guardar</button>';
                        echo "</form>";
                      } else 
                        echo '<div class="checkbox"> <label><input type="checkbox" value="1" name="admin">Administrador</label></div>';

								        
                    ?>

              </div>
            </div>
            <?php endif; ?>


              <div class="panel panel-primary">
                <div class="panel-heading">Dados Pessoais</div>
                <div class="panel-body">

                  <?php if($novoUtilizador==0): ?>
										<form action="<?php if($novoUtilizador==0) echo '?id=' . $_GET['id'] . '&action=alteraDadosPessoais'?>" method="POST">
								<?php endif; ?>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-2">
                          <img id="img" class="img-responsive" src="<?php echo $perfil[8] ?>" onError="this.src = 'img/errorImage.png'; " />
                        </div>
                         <?php if($_SESSION["admin"] == 1 || $_GET['id']==$_SESSION["idUtilizador"]): ?>
                        <div class="col-md-10">
                          <label for="nome">URL Fotografia</label>
                          <input type="text" class="form-control" id="urlFoto" name="urlFoto" placeholder="Nome" value="<?php echo $perfil[8] ?>" required="true" onfocusout="actualizaImagem()">
                        </div>
                         <?php endif; ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="nome">Nome</label>
                      <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?php echo $perfil[0] ?>" required="true">
                    </div>

                    <?php if($_SESSION["admin"] == 1 || $_GET['id']==$_SESSION["idUtilizador"] || $amizade==1): ?>

                    <div class="form-group">
                      <label for="morada">Morada</label>
                      <input type="text" class="form-control" id="morada" name="morada" placeholder="Morada" value="<?php echo $perfil[1] ?>" required="true">
                    </div>

                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" value="<?php echo $perfil[2] ?>" required="true">
                    </div>

                    <div class="form-group">
                      <label for="dataNascimento">Data de Nascimento</label>
                      <input type="date" class="form-control" id="dataNascimento" name="dataNascimento" placeholder="Data de Nascimento" value="<?php echo $perfil[3] ?>" required="true">
                    </div>


                    <div class="form-group">
                      <label>Sexo</label>
                      <div class="radio">
                        <label>
                          <input type="radio" name="sexo" value="H" <?php if($perfil[4]=='H' ) echo "checked" ?>> Masculino</label>
                      </div>

                      <div class="radio">
                        <label>
                          <input type="radio" name="sexo" value="M" <?php if($perfil[4]=='M' ) echo "checked" ?> > Feminino</label>
                      </div>
                    </div>


                    <div class="form-group">
                      <label for="descricao">Descricao</label>
                      <textarea class="form-control" id="descricao" name="descricao" rows="5"><?php echo $perfil[5] ?></textarea>
                    </div>     


                     <?php 
                        endif; 

                        if(($_SESSION["admin"] == 1 || $_GET['id']==$_SESSION["idUtilizador"]) && $novoUtilizador==0)
                            echo '<button type="submit" class="btn btn-primary pull-right">Guardar</button>';

                        if($novoUtilizador==0)
                          echo "</form>";
                     ?>
                </div>
              </div>

<?php
				if($novoUtilizador==1){
					echo '<button type="submit" class="btn btn-primary pull-right">Guardar</button>';
					echo '</form></br></br></br>';
				}
		?>


            <?php if($_SESSION["admin"] == 1 && $novoUtilizador==0): ?>
            <div class="panel panel-danger">
              <div class="panel-heading">Eliminar Conta</div>
              <div class="panel-body">
                <form action="<?php echo '?id=' . $_GET['id'] . '&action=eliminaUtilizador'?>" method="POST">
                   <button type="submit" class="btn btn-danger pull-right">Eliminar</button>
                </form>
              </div>
            </div>
            <?php endif; ?>



        </div>
      </div>
    </div>

</body>

</html>