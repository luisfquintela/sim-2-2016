<nav class="navbar navbar-dark bg-primary navbar-fixed-top">
		<div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">ULHTBook</a>
		    </div>
				 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">				
					<ul class="nav navbar-nav navbar-right">
									<li><a href="listaUtilizadores.php">Utilizadores</a></li>
									<li><a href="listaAmigos.php">Amigos</a> 
									<li><a href="pedidosAmizade.php">Amizade</a>
									<?php if($_SESSION["admin"]==1): ?> <li><a href="perfil.php?action=novoUtilizador">Inserir utilizador</a></li> <?php endif; ?>


									<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
													<?php
													require("baseDados.php");
													
													$pdo = ligacaoBD();
													$result = daNome($pdo,$_SESSION["idUtilizador"]);
													terminaLigacaoBD($pdo);

													echo $result;
												?>
												<span class="caret"></span>
											</a>

											<ul class="dropdown-menu">
												<li><a href="perfil.php?id=<?php echo $_SESSION["idUtilizador"] ?>">Perfil</a></li>
												<li role="separator" class="divider"></li>
												<li><a href="?action=terminarSessao">Terminar Sessão</a></li>
											</ul>
									</li> 
							</ul>
				 </div>
	  	</div>
	</nav>

	<br /><br /><br /><br />


	<?php

		if (isset($_GET['action']) && $_GET['action'] == 'terminarSessao') {
			require('util.php');
			terminarSessao();
		}