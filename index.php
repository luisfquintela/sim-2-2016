<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ULHTBook</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    
    <!-- MENU -->
  	<?php
			session_start();
			require("navLogin.php");
		?>

		<div class="jumbotron">
			<div class="container">
				<br /><br /><br /><br />
				<div class="col-md-6">
					<img class="img-responsive" src="img/social-network.png" />
				</div>
				<div class="col-md-6">
					<h2 class="text-center text-primary">A rede social da ULHT</h1>
					<br /><br />
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Número</th>
								<th>Nome</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>20064896</td>
								<td>Carlos Cunha</td>
							</tr>
							<tr>
								<td>21604345</td>
								<td>Luis Quintela</td>
							</tr>
							<tr>
								<td>21403301</td>
								<td>Rui Correia</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br /><br /><br /><br />
			</div>	
		</div>	


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>