<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Utilizadores</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    
      <?php

        session_start();

        //Se n?o existir login
        if(empty($_SESSION['idUtilizador'])){
          require("navLogin.php");
          require("baseDados.php");
          echo "<br /><br /><br /><br />";
        } else {
          require("nav.php");
        }
              

        

        $pdo = ligacaoBD();
        $todosUtilizadores = daTodosUtilizadores($pdo);
        terminaLigacaoBD($pdo);
		?>



  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nome</th> 
                <th>Imagem</th>
                <?php
                  if($_SESSION["admin"] == 1){ //Se for administrador
                    echo "<th>Telefone</th>";
                    echo "<th>Descrição</th>";
                    echo "<th>Morada</th>";
                    echo "<th>Email</th>";
                    echo "<th>Sexo</th>";
                  }
                ?>   
                <th>Ver Perfil</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($todosUtilizadores as $utilizador){
                  if($utilizador["IDUtilizador"] != $_SESSION['idUtilizador']){
                    echo "<tr>";
                    echo "<td>".$utilizador["Nome"]."</td>";
					          echo "<td><img src=".$utilizador["Imagem"]." height='80' width='80'border='1'></td>";
                    if($_SESSION["admin"] == 1){ //Se for administrador
                      echo "<td>".$utilizador["Telefone"]."</td>";
                      echo "<td>".$utilizador["Descricao"]."</td>";
                      echo "<td>".$utilizador["Morada"]."</td>";
                      echo "<td>".$utilizador["Email"]."</td>";
                      echo "<td>";
                      if($utilizador["Sexo"]=="H")
                        echo "Masculino";
                      else
                        echo "Feminino";
                      echo "</td>";		  
                    } 	  
                    echo "<td><a href='perfil.php?id=".$utilizador["IDUtilizador"]."'><button class='btn btn-default'>Ver Perfil</button></a></td>";					
                  echo "</tr>";
                   }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>